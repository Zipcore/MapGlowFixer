#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

#define PL_VERSION "1.0"

public Plugin myinfo = 
{
	name        = "Map Glow Fix",
	author		= ".#Zipcore",
	description = "",
	version     = PL_VERSION,
	url         = "zipcore#googlemail.com"
};

public void OnEntityCreated(int iEntity, const char[] classname)
{
	if(HasEntProp(iEntity, Prop_Send, "m_flGlowMaxDist") || HasEntProp(iEntity, Prop_Send, "m_nGlowStyle"))
		SDKHook(iEntity, SDKHook_Spawn, OnSpawned);
}

public void OnSpawned(int iEntity)
{
	SDKUnhook(iEntity, SDKHook_Spawn, OnSpawned);
	
	if(HasEntProp(iEntity, Prop_Send, "m_flGlowMaxDist"))
		SetEntPropFloat(iEntity, Prop_Send, "m_flGlowMaxDist", 10000000.0);
	
	if(HasEntProp(iEntity, Prop_Send, "m_nGlowStyle"))
		SetEntProp(iEntity, Prop_Send, "m_nGlowStyle", 0);
}
